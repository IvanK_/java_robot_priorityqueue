import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.PriorityQueue;

public class RobotFactory {

    public static ArrayList<Robot> makeRobots (Integer quantity, String name, Date date, Integer iq, Integer xc, Integer yc) {
        ArrayList<Robot> list = new ArrayList<Robot>();
        for (int i=0; i<quantity; i++) {
            Robot r = new Robot(name + "-" + i, date, iq, xc, yc);
            list.add(r);
        }
        return list;
    }

    public static void showRobotsList(ArrayList<Robot> list) {
        for (int i=0; i<list.size(); i++)
            System.out.println(list.get(i));
    }

    public static PriorityQueue<Robot> prioritize (ArrayList<Robot> list) {
        Comparator<Robot> comparator = new Comparator<Robot>() {
            public int compare(Robot r1, Robot r2) {
                Integer iq_diff = r1.getIq() - r2.getIq();
                int result = 0;
                if (iq_diff > 0) result=-1;
                if (iq_diff < 0) result=1;
                if (iq_diff == 0) result=0;
                return result;
            }
        };
        PriorityQueue<Robot> pq_list = new PriorityQueue<Robot>(list.size(), comparator);
        pq_list.addAll(list);
        return pq_list;
    }
}
