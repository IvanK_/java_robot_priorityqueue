import javax.swing.text.html.HTMLDocument;
import java.text.SimpleDateFormat;
import java.util.*;

public class App {

    public static void main(String[] args) {

        ArrayList<Robot> all_robots = new ArrayList<Robot>();
        Date date_1 = new Date();
        Date date_2 = new Date();

        try {
            date_1 = new SimpleDateFormat( "yyyy.MM.dd" ).parse( "2018.01.01" );
            date_2 = new SimpleDateFormat( "yyyy.MM.dd" ).parse( "2018.06.06" );

        } catch (Exception e) {}

        ArrayList<Robot> robots_part_1 = RobotFactory.makeRobots(3, "Standard Mind", date_2, 75, 0,0 );
        ArrayList<Robot> robots_part_2 = RobotFactory.makeRobots(3, "Super Clever", date_1, 100,1,1 );
        ArrayList<Robot> robots_part_3 = RobotFactory.makeRobots(3, "Need Approve", date_2, 50,2,2 );

        all_robots.addAll(robots_part_1);
        all_robots.addAll(robots_part_2);
        all_robots.addAll(robots_part_3);

        System.out.println("Ordinary Array List of Robots:");
        RobotFactory.showRobotsList(all_robots);
        System.out.println();

        PriorityQueue<Robot> prioritized_robots = RobotFactory.prioritize(all_robots);
        Iterator<Robot> iterator = prioritized_robots.iterator();

        System.out.println("PriorityQueue of Robots:");
        while (iterator.hasNext()) {
            Robot robot = iterator.next();
            System.out.println(robot);
        }

        System.out.println( "Test distance between 2 robots");

        Robot one_r = new Robot("Robot One", date_1, 30, 0,0 );
        Robot two_r = new Robot("Robot Two", date_2, 35, 5,5 );
        System.out.println("Distance is: " + Robot.distanceBetween(one_r, two_r));
    }
}
