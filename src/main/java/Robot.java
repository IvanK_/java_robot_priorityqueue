import java.util.Date;
public class Robot implements Comparable {

    private String name;
    private Date manufactured;
    private Integer iq;
    private Integer x;
    private Integer y;

    public Robot(String name, Date manufactured, Integer iq, Integer x, Integer y) {
        setName(name);
        setManufactured(manufactured);
        setIq(iq);
        setX(x);
        setY(y);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getManufactured() {
        return manufactured;
    }

    public void setManufactured(Date manufactured) {
        this.manufactured = manufactured;
    }

    public Integer getIq() {
        return iq;
    }

    public void setIq(Integer iq) {
        this.iq = iq;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "name: " + name +
                ", date manufactured: " + manufactured +
                ", IQ: " + iq +"}";
    }

    public int compareTo(Object robot) {
        Robot cast_robot = (Robot) robot;
        Integer iq_diff = this.getIq() - cast_robot.getIq();
        int result = 0;
        if (iq_diff > 0) result=1;
        if (iq_diff < 0) result=-1;
        if (iq_diff == 0) result=0;
        return result;
    }

    public static double distanceBetween(Robot robot_1, Robot robot_2) {
        double distance=0;
        int x_this = robot_1.getX();
        int y_this = robot_1.getY();
        int x = robot_2.getX();
        int y = robot_2.getY();
        distance = Math.sqrt ( Math.pow(x-x_this,2) + Math.pow(y-y_this,2));
        return distance;
    }
}
